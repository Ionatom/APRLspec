#' CopyPkgFiles
#'
#' Copy package files (e.g., from \code{inst/}) to desired location.
#'
#' @param package [character] Name of package.
#' @param files [character] Name of package files.
#' @param path [character] Location to copy files. If present directory (\code{"."}) or directory exists, only contents are copied. Otherwise, a directory of this name is created and contents are copied there.
#' @param addargs [character] Arguments to rsync for \code{SyncPkgFiles}. Unused by \code{CopyPkgFiles}.
#'
#' @return Side effect of copying example files to \code{path}.
#' @examples CopyPkgFiles("APRLmpf", "example_files", ".", "mpf")
#' @export

CopyPkgFiles <- function(package, files, path=".", prefix=NULL, recursive=FALSE) {
  
  ## -----------------------------------------------------------------------------
  
  if(is.character(prefix)) {
    
    AddPrefix <- function(f) sprintf("%s_%s", prefix, f)
    
    CopyFile <- function(remote, local, temporary=".tmp_PkgfilesCopy") {
      
      remote.isdir <- file.info(remote)[["isdir"]]
      local.isdir <- file.info(local)[["isdir"]]
      
      if(!remote.isdir && !local.isdir) {
        
        file.copy(remote, local)
        
      } else if(!remote.isdir && local.isdir) {
        
        file.copy(remote, file.path(local, AddPrefix(basename(remote))))
        
      } else if(remote.isdir && local.isdir) {
        print("here2")
        .local <- file.path(temporary, local)
        file.copy(remote, .local, recursive=TRUE)
        file.rename(file.path(.local, basename(remote)), AddPrefix(basename(remote)))
        
      } else {
        
        NULL
        
      }
      
    }
    
  } else {
    
    CopyFile <- function(from, to, temporary, ...) {
      file.copy(from, to, recursive = recursive, ...)
      
    }
    
  }
  
  CopyContents <- function(remote, local, temporary=".tmp_PkgfilesCopy", ...) {
    
    if(file.info(remote)[["isdir"]]) {
      
      dir.create(temporary)#, showWarnings=FALSE)
      
      for(f in Sys.glob(file.path(remote, "*")))
        CopyFile(f, local, temporary)
      
      file.remove(temporary)
      
    } else {
      
      CopyFile(remote, local)
      
    }
  }
  
  ## -----------------------------------------------------------------------------
  
  location <- system.file(files, package=package)
  
  if(location=="") {
    message("no match, returning NULL")
    return(NULL)
  }
  
  if(file.info(location)[["isdir"]] && !file.exists(path))
    dir.create(path, recursive=TRUE)
  
  CopyContents(location, path)
  
}

