
#' Dict2Table, Table2Dict
#'
#' Convert between named vector and data frame
#'
#' @param x [data.frame, vector]
#' @param index [character]
#' @param value.var [character]
#'
#' @return Either a data frame or named vector
#' @export

Dict2Table <- function(x, index="id", value.var="value") {
  if(is.null(names(x)))
    names(x) <- x
  as.data.frame(setNames(list(names(x), x), c(index, value.var)), optional=TRUE)
}

#' @describeIn Dict2Table reverse operation
#' @export

Table2Dict <- function(x, index="id", value.var="value") {
  if(ncol(x)==1) {
    setNames(unlist(x), row.names(x))
  } else {
    setNames(x[[value.var]], x[[index]])
  }
}
