
#' SetLabel
#'
#' Generate labels and name vector according to prefix.
#'
#' @param x object
#' @param prefix [character] character
#' @param margin [integer, optional] margin number
#' @param idx indices
#'
#' @return Named vector/matrix/list.
#' @export

SetLabel <- function(x, prefix, margin=0) {
  if(margin < 1) {
    names(x) <- GenLabel(prefix, seq_along(x))
  } else {
    dimnames(x)[[margin]] <- GenLabel(prefix, seq(dim(x)[margin]))
  }
  x
}

#' @describeIn SetLabel Generates labels to be used by \code{SetLabels}.
#' @export

GenLabel <- function(prefix, idx)
  sprintf("%s%d", prefix, idx)

#' @describeIn SetLabel Check if vector names conform to label format.
#' @export

IsLabel <- function(x, prefix) {
  !is.null(x) &&
    all(substring(x, 1, 1)==prefix) &&
    !any(is.na(suppressWarnings(as.integer(substring(x, 2)))))
}
