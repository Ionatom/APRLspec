ATTR.WAVENUMBERS <- "wavenum"
ATTR.DESCRIPTION <- "description"
ATTR.SPEC <- c(ATTR.WAVENUMBERS, ATTR.DESCRIPTION)

## -----------------------------------------------------------------------------

#' Spec
#'
#' Methods for class \code{Spec} for storing a spectrum/spectra.
#'
#' Implemented as a numeric matrix with a "wavenum" attribute containing wavenumbers. \code{`rownames<-`} will preserve attributes, but primitive functions \code{`[`} and \code{rbind} will lead to their loss. Therefore, \code{Slice} and \code{SBind} methods have been defined in their place, respectively.
#'
#'
#' @param x matrix or vector of absorbance/transmission values, or a \code{SpecDB} object
#' @param wavenum wavenumbers
#' @param tbl names of spectra and wavenumber tables (vector of length 2)
#' @param subset vector of sample IDs to extract
#'
#' @return \code{Spec}: An object of class \code{Spec}.
#' @export

Spec <- function(x, ...)
  UseMethod("Spec")

#' @rdname Spec
#' @export

Spec.numeric <- function(x, wavenum, description=NULL) {
  if(length(x)!=length(wavenum))
    stop("[USER ERROR]: Dimensions must match.")
  x <- matrix(x, nrow=1, dimnames=list(as.character(match.call()[2]), names(x)))
  if(!IsLabel(dimnames(x)[[2]], "W"))
    dimnames(x)[[2]] <- GenLabel("W",seq(dim(x)[2]))
  class(x) <- union("Spec", class(x))
  Wavenumbers(x) <- wavenum
  Description(x) <- description
  x
}

#' @rdname Spec
#' @export

Spec.matrix <- function(x, wavenum, description=NULL) {
  if(mode(x)!="numeric")
    stop("[USER ERROR]: Must be numeric matrix")
  if(is.null(dimnames(x)[[1]]))
    dimnames(x)[[1]] <- GenLabel("s",seq(dim(x)[1]))
  if(!IsLabel(dimnames(x)[[2]], "W"))
    dimnames(x)[[2]] <- GenLabel("W",seq(dim(x)[2]))
  class(x) <- union("Spec", class(x))
  Wavenumbers(x) <- wavenum
  Description(x) <- description
  x
}

#' @rdname Spec
#' @export

Spec.SpecDB <- function(db, tbl=c("spectra", "wavenumber"), subset) {
  ## subset is a named list of length 1
  ## example: list("sample"=c("MEVEX_20110101", "MEVEX_20110103"))
  if(missing(subset)) {
    spec <- as.matrix(ReadTable(db, tbl[1]))
  } else {
    table <- db$tables[[tbl[1]]]
    tblname <- table$name
    column <- table$columns[["primary"]]
    values <- paste(FormatElems(subset), collapse=",")
    query <- sprintf("SELECT * IN %s WHERE %s in (%s)",
                     tblname, column, values)
    spectable <- dbGetQuery(db$conn, query)
    spec <- as.matrix(Rfunctools::SetIndex(spectable, column))
  }
  wavenum <- ReadDictTable(db, tbl[2])
  Spec(spec, wavenum[dimnames(spec)[[2]]])
}


#' @rdname Spec
#' @return \code{is.Spec}: Test if Spec class (returns logical).
#' @export

is.Spec <- function(x)
  inherits(x, "Spec")

#' @rdname Spec
#' @return \code{as.Spec}: Generic function.
#' @export

as.Spec <- function(x, ...) 
  UseMethod("as.Spec")

#' @rdname Spec
#' @export

as.Spec.default <- function(x, ...) {
  if(ATTR.WAVENUMBERS %in% names(attributes(x))) {
    class(x) <- union("Spec", class(x))
  } else {
    stop("[USER ERROR]: requires wavenumber specification")
  }
  x
}

#' @rdname Spec
#' @return \code{as.matrix}: Drops \code{Spec} attributes (note: loses wavenumbers).
#' @export

as.matrix.Spec <- function(x, ...) {
  x <- unclass(x)
  attributes(x)[ATTR.SPEC] <- NULL
  x
}


## -----------------------------------------------------------------------------

#' `[.Spec`
#'
#' Subsetting method for \code{Spec} objects (supercedes \code{Slice}). Inherits from \code{matrix}.
#'
#' @param ... arguments passed to \code{base::`[`}
#' @param drop [logical] drop dimensions. \code{FALSE} by default in contrast to \code{matrix} class.
#'
#' @return Subsetted \code{Spec} object, unless dimensions are dropped (\code{drop=TRUE}).
#' @export

`[.Spec` <- function(x, ..., drop = FALSE) {
  xnew <- base::`[`(unclass(x), ..., drop = drop)
  if(!is.null(dim(xnew))) {  # necessary for str()
    class(xnew) <- class(x)    
    Wavenumbers(xnew) <- Wavenumbers(x)[dimnames(xnew)[[2]]]
  }
  xnew
}

## -----------------------------------------------------------------------------

#' Spectrum
#'
#' Obtain single spectrum: list with elements \code{x} and \code{y}.
#'
#' @param x object
#'
#' @return [list] with wavenumbers (\code{x}) and value (\code{y}).
#' @export
Spectrum <- function(x)
  UseMethod("Spectrum")

#' @rdname Spectrum
#' @export
Spectrum.Spec <- function(x)
  list(x=Wavenumbers(x), y=x[1,,drop=TRUE])


## -----------------------------------------------------------------------------

#' Wavenumbers, `Wavenumbers<-`, SetWavenumbers
#'
#' Wavenumber operations.
#'
#' @param x Spec or arbitrary object
#' @param value wavenumbers
#'
#' @name Wavenumbers
NULL

#' 
#' @rdname Wavenumbers
#' @return Wavenumbers or Spec object with wavenumbers attached.
#' @export
Wavenumbers <- function(x, ...)
  UseMethod("Wavenumbers")

#' @rdname Wavenumbers
#' @method Wavenumbers default
#' @export
Wavenumbers.default <- function(x)
  attr(x, ATTR.WAVENUMBERS)

#' @rdname Wavenumbers
#' @method Wavenumbers Spec
#' @export
Wavenumbers.Spec <- function(x)
  attr(x, ATTR.WAVENUMBERS)

#' @rdname Wavenumbers
#' @export
`Wavenumbers<-` <- function(x, ...)
  UseMethod("Wavenumbers<-")

#' @rdname Wavenumbers
#' @method "Wavenumbers<-" default
#' @export
`Wavenumbers<-.default` <- function(x, value) {
  ## attach to arbitrary object
  ## if(!IsLabel(names(value), "W"))
  ##   names(value) <- GenLabel("W", seq_along(value))
  attr(x, ATTR.WAVENUMBERS) <- value  
  x
}

#' @rdname Wavenumbers
#' @method "Wavenumbers<-" Spec
#' @export
`Wavenumbers<-.Spec` <- function(x, value) {
  if(dim(x)[2]!=length(value))
    stop("[USER ERROR]: Dimensions must match.")
  names(value) <- dimnames(x)[[2]]
  attr(x, ATTR.WAVENUMBERS) <- value
  x
}

#' @rdname Wavenumbers
#' @export
SetWavenumbers <- function(x, ...)
  UseMethod("SetWavenumbers")

#' @rdname Wavenumbers
#' @method SetWavenumbers default
#' @export
SetWavenumbers.default <- function(x, value) {
  Wavenumbers(x) <- value
  x
}

## -----------------------------------------------------------------------------

#' Description, `Description<-`, SetDescription
#'
#' Assign and get Description tables.
#'
#' `rownames` of Spec matrix and `row.names` of Description data frame must match.
#'
#' @param x Spec object
#' @param value Description table
#'
#' @return Description table or Spec object.
#' @export

Description <- function(x, ...)
  UseMethod("Description")

#' @rdname Description
#' @export

Description.Spec <- function(x)
  attr(x, ATTR.DESCRIPTION)

#' @rdname Description
#' @export

`Description<-` <- function(x, ...)
  UseMethod("Description<-")

#' @rdname Description
#' @export

`Description<-.default` <- function(x, value) {
  if(!is.null(value)) {
    if(!isTRUE(all.equal(rownames(x) %in% row.names(value))))
      stop("[USER ERROR]: Row names must match.")
    attr(x, ATTR.DESCRIPTION) <- value
  }
  x
}

#' @rdname Description
#' @export

`Description<-.Spec` <- `Description<-.default`

#' @rdname Description
#' @export

SetDescription <- function(x, ...)
  UseMethod("SetDescription")

#' @rdname Description
#' @export

SetDescription.Spec <- function(x, value) {
  Description(x) <- value
  x
}

## -----------------------------------------------------------------------------

#' plot, print, summary
#'
#' Thin wrappers for matplot, print, and summary.
#'
#' @param x Spec object
#'
#' @name Wrappers

NULL

#' @rdname Wrappers
#' @export

plot.Spec <- function(x, ..., xlim=rev(range(Wavenumbers(x))), type="l", xlab=expression("Wavenumber"~(cm^-1)), ylab="Absorbance")
  matplot(Wavenumbers(x), t(x), xlim=xlim, type=type, xlab=xlab, ylab=ylab, ...)

#' @rdname Wrappers
#' @export

lines.Spec <- function(x, ...)
  matlines(Wavenumbers(x), t(x), ...)

#' @rdname Wrappers
#' @export

print.Spec <- function(x, ...)
  print(str(x))

#' @rdname Wrappers
#' @export

summary.Spec <- function(x, ...)
  setNames(dim(x), c("samples", "wavenumbers"))

## -----------------------------------------------------------------------------

#' Reshape
#'
#' Melt, Scast. Reshape \code{Spec} matrix and \code{SpecDF} data frame (uses \code{reshape2} package).
#'
#' @param x \code{Spec} matrix or \code{SpecDF} data frame
#' @param formula formula
#' @param wavenum [numeric] or [character] wavenumber vector or name of columns (vector of length 2) that contain the wavenumber index and wavenumber values
#' @param ... arguments to be passed to \code{reshape2::melt} or \code{reshape2::acast}
#'
#' @name Reshape
NULL

## #' @rdname Reshape
## #' @export

## Melt <- function(x, ...)
##   UseMethod("Melt")

#' @describeIn Reshape \code{Spec} matrix --> \code{SpecDF} data frame
#' @export

melt.Spec <- function(x, varnames=c("sample", "iw"), value.name="spec", as.is=TRUE, ...) { # could also be assigned to melt.Spec
  lf <- reshape2::melt(unclass(x), varnames=varnames, value.name=value.name, as.is=as.is, ...)
  lf$wavenum <- Wavenumbers(x)[lf$iw]
  class(lf) <- c("SpecDF", class(lf))
  lf
}

#' @describeIn Reshape \code{SpecDF} data frame --> \code{Spec} matrix
#' @export

scast <- function(x, formula=sample~iw, wavenum=c("iw", "wavenum"), value.var="spec", ...) {
  if(mode(wavenum)=="character") {
    wv <- unique(x[wavenum])
    wavenum <- sort(setNames(wv[,2], wv[,1]), decreasing=TRUE)
  }
  mat <- reshape2::acast(x, formula=formula, value.var=value.var, ...)
  Spec(mat[,names(wavenum)], wavenum)
}

## -----------------------------------------------------------------------------

#' SpecApply, SpecPGrid
#'
#' Apply function for single spectrum row-wise to spectra matrix. SpecPGrid additionally explores Cartesian products of additional function arguments - vector inputs that should be fixed for all runs should be set using \code{purrr::partial}.
#'
#' @param x [Spec] spectra
#' @param FUN [function] function for single spectrum
#' @param ... arguments to be passed to \code{FUN}
#' @param SIMPLIFY [logical] try to simplify output
#'
#' @name SpecApply
NULL

#' @rdname SpecApply
#' @return \code{SpecApply}: assembled output (typically \code{Spec} matrix or other matrix)
#' @export
#' 
SpecApply <- function(x, FUN, ..., SIMPLIFY=TRUE) {
  ## "simplify" is implied

  ## This is not efficient especially if there are a lot of spectra
  ##   e.g., Reduce(SBind, ...)
  ##
  ## Can be improved by writing separate functions for anticipated outputs
  ##
  ##   Examples:
  ##
  ##   1. resulting spectra have same dimensions as original
  ##
  ##   xnew <- x
  ##   xnew[] <- t(apply(x, 1, FUN, ...))
  ##
  ##   2. all spectra have same dimensions but different from original
  ##
  ##   Spec(Reduce(rbind, speclist), Wavenumbers(speclist[[1]]))
  ##

  ## the following loses the Spec class required by FUN
  ##
  ## xnew <- lapply(setNames(split(x, row(x)), rownames(x)), FUN, ...)

  ## preserve `Spec` class with x[.sample,]
  xnew <- lapply(setNames(,rownames(x)), function(i) FUN(x[i,], ...))

  if(SIMPLIFY) {
    if(is(xnew[[1]], "Spec")) {
      out <- SBind(xnew)
    } else if(is(xnew[[1]], "PeakParam")) {
      out <- do.call(rbind, xnew)
      class(out) <- class(xnew[[1]])
    } else {
      out <- xnew
    }
  } else {
    out <- xnew
  }

  out
  
}

#' @rdname SpecApply
#' @return \code{SpecPGrid}: 3D list array of {sample, parameter combination, output value}
#' @export
#'
SpecPGrid <- function(x, FUN, ...) {
  ##
  ## Applies FUN to all samples in spectra using parameters ...
  ## Returns 3D list array of {sample, param, value}
  ##
  pseq <- do.call(expand.grid, c(list(...), stringsAsFactors=FALSE))
  row.names(pseq) <- GenLabel("P", seq(nrow(pseq)))
  ##
  grid <- list(sample = rownames(x), param = row.names(pseq))
  cprod <- do.call(expand.grid, c(grid, stringsAsFactors=FALSE))
  ##
  w <- Wavenumbers(x)  
  out <- mapply(function(s, p) do.call(FUN, c(list(w, x[s,,drop=TRUE]), pseq[p,])),
                cprod$sample, cprod$param)
  ##
  ## matrix{value, sample x param} -> array{sample, param, value}
  arr <- array(aperm(out, 2:1), # "transpose"
               dim = c(sapply(grid, length), value = dim(out)[1]),
               dimnames = c(grid, value = dimnames(out)[1]))
  Wavenumbers(arr) <- w
  attr(arr, "param") <- pseq
  arr
}

#' @rdname SpecApply
#' @return \code{SpecFUN}: function that takes a single spectrum function (x, y) and returns a function that takes a spec object
#' @export
SpecFUN <- function(FUN) 
  function(x, ...) do.call(FUN, c(unname(Spectrum(x)), list(...)))


## -----------------------------------------------------------------------------
## examples

## m <- matrix(1:30, nrow=3, dimnames=list(1:3, NULL))

## m <- Spec(m, 1:ncol(m))

## Slice(m, 1:2)

## Slice(m, 1, 2:3)

## SetWavenumbers(slice(m, 1, 2:3))

## SBind(m, 1:3, 2)

## SBind(m, 1:10)

## plot(m)

## print(m)
