
#' SetIndexKey, ListKeys, AsPrimKey
#'
#' Key operations.
#'
#' @param db SpecDB object
#' @param type SQL data type
#' @param x vector of character strings
#'
#' @name KeyOps

NULL

#' @rdname KeyOps
#' @export


SetIndexKey <- function(x, ...)
  UseMethod("SetIndexKey")

#' @return \code{SetIndexKey}: Sets primary key a posteriori in SQLite.
#'
#' @rdname KeyOps
#' @export

SetIndexKey.SpecDB <- function(db, tbl) {
  table <- db$tables[[tbl]]
  uniqueIndex <- column <- names(table$columns[["primary"]])
  tblname <- table$name
  command <- sprintf("CREATE UNIQUE INDEX IF NOT EXISTS %s ON %s (%s)",
                     uniqueIndex, tblname, column) ## P_Id
  dbSendQuery(db$conn, command)
}

#' @rdname KeyOps
#' @export

ListKeys <- function(x, ...)
  UseMethod("ListKeys")

#' @return \code{ListKeys}: Vector of primary keys from table.
#'
#' @rdname KeyOps
#' @export

ListKeys.SpecDB <- function(db, tbl) {
  key <- NULL # query the key
  query <- sprintf("SELECT %s FROM %s", key, tbl)
  unname(unlist(dbGetQuery(db$conn, query)))
}

#' @return \code{AsPrimKey}: Augments field to be Primary Key.
#'
#' @rdname KeyOps
#' @export

AsPrimKey <- function(type, key=TRUE)
  replace(type, key, paste(type, "PRIMARY KEY"))

#' @return \code{CompositeKey}: Create composite key from elements.
#'
#' @rdname KeyOps
#' @export
#'
#' @examples
#' CompositeKey(c("a", "b"))
#'   # -> (a,b)

CompositeKey <- function(x)
  sprintf("(%s)", paste(x, collapse=","))
