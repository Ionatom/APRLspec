ATTR.WSCALE <- "wscale"
ATTR.WAVENUMBERS <- "wavenum"
ATTR.DESCRIPTION <- "description"
WTOL <- 1e-9 # WTOL > .Machine$double.eps, .Machine$double.neg.eps; empirically determined from spectra with 11138 wavenumbers
ATTR.SPECW <- c(ATTR.WSCALE, ATTR.DESCRIPTION)

#' SpecW object
#'
#' Similar to \code{Spec} but further imposes that wavenumbers are contiguous. Modeled after SpecW object in Igor Pro.
#'
#' @param x object
#' @param wavenum,value wavenumbers
#'
#' @name SpecW
NULL

#' @rdname SpecW
#' @export

SpecW <- function(x, ...)
  UseMethod("SpecW")

#' @rdname SpecW
#' @export

SpecW.default <- function(x, ...)
  if(is.Spec(x)) as.SpecW(x) else as.SpecW(Spec(x, ...))

#' @rdname SpecW
#' @export

`[.SpecW` <- function(x, ..., drop = FALSE) {
  xnew <- base::`[`(unclass(x), ..., drop = drop)
  if(!is.null(dim(xnew))) {  # necessary for str()
    class(xnew) <- class(x)    
    Wavenumbers(xnew) <- Wavenumbers(x)[dimnames(xnew)[[2]]]
  }
  xnew
}

#' @rdname SpecW
#' @export

Wavenumbers.SpecW <- function(x) {
  w <- attr(x, ATTR.WSCALE)
  setNames(seq(w[1], w[2], , dim(x)[2]), dimnames(x)[[2]])
}

#' @rdname SpecW
#' @export

`Wavenumbers<-.SpecW` <- function(x, value) {
  if(dim(x)[2]!=length(value))
    stop("[USER ERROR]: Dimensions must match.")
  dw <- diff(value)
  if(any(abs(dw-dw[1]) > get("WTOL", environment())))
    stop("[USER ERROR]: Wavenumbers are not contiguous.")
  names(value) <- NULL
  attr(x, ATTR.WSCALE) <-
    c(head(value, 1), tail(value, 1))
  x
}

#' @rdname SpecW
#' @export

is.SpecW <- function(x)
  inherits(x, "SpecW")

#' @rdname SpecW
#' @export

as.SpecW <- function(x, ...) {
  if(is.Spec(x)) {
    xnew <- unclass(x)
    Wavenumbers(xnew) <- NULL
    class(xnew) <- union(c("SpecW", "Spec"), class(x))
    Wavenumbers(xnew) <- Wavenumbers(x)
  } else {
    xnew <- SpecW(x, ...)
  }
  xnew
}

#' @rdname SpecW
#' @export

as.Spec.SpecW <- function(x, ...) 
  Spec(as.matrix(x), Wavenumbers(x))  

#' @rdname SpecW
#' @export

Stitch.SpecW <- function(x, ...) 
  Stitch.Spec(x, ..., align=TRUE)

#' @rdname SpecW
#' @export

as.matrix.SpecW <- function(x, ...) {
  x <- unclass(x)
  attributes(x)[ATTR.SPECW] <- NULL
  x
}
