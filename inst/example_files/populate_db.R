


defaults <- list(
  "reader"   = NULL,    ## relies on file content perception
  "dbconfig" = list(
    "driver" = "SQLite", ## currently only option
    "file"   = "./spectra.sqlite",
    "tables" = "$extdata/dbspec_network.json"
)

## -----------------------------------------------------------------------------

options(stringsAsFactors=FALSE)

## -----------------------------------------------------------------------------

library(Rfunctools)
library(APRLspec)
library(RSQLite)
library(RJSONIO)
library(reshape2)
library(dplyr)
library(gsubfn)

## -----------------------------------------------------------------------------

argv <- ParseArgs()
userinput <- argv$args[1]
srcpath <- dirname(argv$file)
runpath <- dirname(argv$args[1])

Path <- ExpandPath(dotslash=runpath, package="APRLspec")

## -----------------------------------------------------------------------------
## User inputs

args <- FillDefaults(fromJSON(userinput), defaults)

## wavenum <- scan(args[["wavenumfile"]], what=0, quiet=TRUE)
wavenum <- unlist(read.csv(args[["wavenumfile"]]), use.names=FALSE)

dbfile <- Path(args[["dbconfig"]][["file"]])
dbtables <- fromJSON(Path(args[["dbconfig"]][["tables"]]))
dbdriver <- args[["dbconfig"]][["driver"]]

## -----------------------------------------------------------------------------

ParseFilename <- function(filename, pattern, fields, defaults) {
  parsed <- setNames(strapplyc(filename, pattern)[[1]], fields)
  FillDefaults(parsed, defaults)
}

Notice <- function(db, tbl, x)
  sprintf("---input error: sample '%s' in table '%s'---",
          x, db$tables[[tbl]]$name)

## -----------------------------------------------------------------------------

db <- SpecDB(dbfile, dbtables, dbdriver)

CreateTable(db, "description")

CreateTable(db, "spectra")

## -----------------------------------------------------------------------------

filelist.db <- Select(db, "description", "filename", as.vec=TRUE)

## -----------------------------------------------------------------------------

sink(file.path(runpath, "db_import_log.txt"), append=TRUE)

cat(as.character(Sys.time()),"\n")

ii <- 1

for(ff in filelist) {

  ## ----- Parse names and create key -----

  fields <- ParseFileName(ff)
  key <- Compositekey(fields)

  ## ----- read spectrum -----

  spectrum <- try(do.call(ReadSpec, c(list(ff), args[["reader"]])), TRUE)

  mssg <- attr(spectrum, "warning")
  if(!is.null(mssg)) {
    cat(Notice(db, "spectra", sample), "\n")
    cat(mssg, "\n")
    next
  }

  spectrum <- Align(spectrum, wavenum)

  ## ----- set command ------

  command <- if(ff %in% filelist.db) "REPLACE" else "INSERT"

  ## ----- insert spectrum ------

  mssg <- try(InsertRow(db, "spectra", key, spectrum, command), TRUE)

  action <- CheckMessage(db, "spectra", sample, mssg)
  action()

  ## ----- insert description table ------

  mssg <- try(InsertRow(db, "description", key, fields, command), TRUE)

  if(inherits(mssg, "try-error")) {
    cat(Notice(db, "description", sample), "\n")
    cat(mssg, "\n")
    DeleteEntry(db, "spectra", key)
    next
  }

  ## ----- done -----

  cat(sprintf("imported '%s'",sample),"\n")

  ii <- ii+1
}

sink()

## -----------------------------------------------------------------------------

ClearResult(db)

Close(db)
