APRLspec
===

You have reached the repository for APRLspec. This R package implements some of the underlying S3 object classes and I/O functions used by [APRLssb](https://aprl.gitlab.io/APRLssb) (baseline correction), [APRLmpf](https://aprl.gitlab.io/APRLmpf) (peak fitting), and [APRLmvr](https://aprl.gitlab.io/APRLmvr) (multivariate calibration).

## Main features

* `Spec`: A class that inherits from the numeric matrix class of base R, but includes additional wavenumber attribute. Methods for extraction, alignment, and merging which preserve wavenumber attributes are included.
* `SpecDB`: An interface for interacting with spectra and metadata in a SQL database.
* Functions for parsing user inputs based on a predetermined set of conventions.

## Installation

The repository mirror is [https://gitlab.com/aprl/APRLspec](https://gitlab.com/aprl/APRLspec) (visible with credentials). Please contact the package author for access.
